import axios from "axios";
axios.defaults.headers.common["x-api-key"] = "hFVfBPDxgrRhtW4NTbOPHAtt";

// CHANGE BASE URL
const httpService = axios.create({
  baseURL: "https://api.storyblok.com/v1/cdn/stories",
});

export default httpService;
