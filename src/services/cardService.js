import httpService from './httpService';
// get cards
const getCards = async () => {
  const {
    data
  } = await httpService.get('/home');
  return data;
};

const cardService = {
  getCards
};

export default cardService;