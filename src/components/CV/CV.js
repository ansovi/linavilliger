import Milestone from "@/components/Milestone/Milestone.vue";
import {
	mapGetters
} from 'vuex';

export default {
	name: "CV",
	components: {
		Milestone
	},
	data() {
		return {};
	},
	computed: {
		...mapGetters(['getMilestones'])
	}
};