export default {
	name: "Process",
	props: {
		windowWidth: 0
	},
	data() {
		return {
			index: 1,
		};
	},
	methods: {
		changeIndex(forward) {
			if (forward) {
				if (this.index != 6) {
					this.index = ++this.index;
				}
			} else {
				if (this.index != 1) {
					this.index = --this.index;
				}

			}
		}
	}
};