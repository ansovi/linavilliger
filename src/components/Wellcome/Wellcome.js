import Favorit from "@/components/Favorit/Favorit.vue";
import {
	mapGetters,
	mapActions
} from 'vuex';

export default {
	name: "Wellcome",
	props: {
		windowWidth: 0,
	},
	components: {
		Favorit
	},
	data() {
		return {};
	},
	created() {
		this.loadFavorits()

	},
	methods: {
		...mapActions(['loadFavorits'])
	},
	computed: {
		...mapGetters(['getFavorits']),
		currentFavorits() {
			let favorits;
			if (this.windowWidth < 800) {
				favorits = this.getFavorits.slice(3)
			} else if (this.windowWidth < 1000) {
				favorits = this.getFavorits.slice(2)
			} else if (this.windowWidth < 1200) {
				favorits = this.getFavorits.slice(1)
			} else {
				favorits = this.getFavorits
			}
			return favorits;
		}
	}
};