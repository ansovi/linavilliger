import Project from "@/components/Project/Project.vue";
import {
	mapGetters
} from 'vuex';

export default {
	name: "Projects",
	props: {
		windowWidth: 0,
	},
	components: {
		Project
	},
	data() {
		return {};
	},
	computed: {
		...mapGetters(['getProjects'])
	}
};