import Oidc from 'oidc-client';

const oidcSettings = {
	authority: 'localhost',
	client_id: 'vuejs_code_client',
	userStore: new Oidc.WebStorageStateStore({
		store: window.localStorage,
	}),
	redirectUri: `${window.location.protocol}//${window.location.host}/oidc/callback`,
	responseType: 'token',
	scope: 'openid roles user_attributes',
	loadUserInfo: false,
};
export default oidcSettings;