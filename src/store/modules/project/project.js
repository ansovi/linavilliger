const state = {
	projects: [{
		id: 1,
		name: "Die Winterhosen",
		description: "Diese Hosen waren eine meiner ersten Hosen, die ich überhaupt genäht und angezogen habe. Ich habe Sie im Winter von 2018 genäht und ich war ziemlich stolz darauf auch im Winter meine eigenen Hosen anhaben zu können. Sie bestehen nämlich aus sehr dichtem Jeansstoff und sind sehr gut geeignet, um auch an kälteren Tagen zu tragen. Mittlerweile habe ich diese Hose sogar schon einmal für eine gute Freundin nachgenäht.",
		imageURL: "http://linavilliger.ch/images/winterhose/winter.png",
		isFavorit: true,
		images: [
			"http://linavilliger.ch/images/winterhose/DSC00260.jpg",
			"http://linavilliger.ch/images/winterhose/DSC00244.jpg",
			"http://linavilliger.ch/images/winterhose/DSC00242.jpg",
		]
	}, {
		id: 2,
		name: "Die Blumenhose",
		description: "Scheint ist verband gar endlich erstieg lockere weg. Spiel das alt knapp bette gru. Licht la schau stand tiefe bello im. Gehts im ob deine dafur es ei. Hab sudwesten gro nebendran ihr schreibet dammerung schlanken erstaunen. Es spielen ab nachtun sondern starkem nachher pa braunen. Da uberall spuckte te an namlich konntet taghell. Marschen funkelte betrubte ja sa lauschte schmalen brauchen. Gesprachig gut mit geheiratet hereintrat angenommen. Wo lief beim leer nein bi ei meer mi.",
		imageURL: "http://linavilliger.ch/images/winterhose/winter.png",
		isFavorit: false,
		images: [
			"http://linavilliger.ch/images/blumenhose/DSC_0264.jpg",
			"http://linavilliger.ch/images/blumenhose/DSC_0278.jpg",
			"http://linavilliger.ch/images/blumenhose/DSC_0286.jpg",
			"http://linavilliger.ch/images/blumenhose/DSC_0295.jpg",
		]
	}, {
		id: 3,
		name: "Die Restenhosen",
		description: "Scheint ist verband gar endlich erstieg lockere weg. Spiel das alt knapp bette gru. Licht la schau stand tiefe bello im. Gehts im ob deine dafur es ei. Hab sudwesten gro nebendran ihr schreibet dammerung schlanken erstaunen. Es spielen ab nachtun sondern starkem nachher pa braunen. Da uberall spuckte te an namlich konntet taghell. Marschen funkelte betrubte ja sa lauschte schmalen brauchen. Gesprachig gut mit geheiratet hereintrat angenommen. Wo lief beim leer nein bi ei meer mi.",
		imageURL: "http://linavilliger.ch/images/restenhose/resten.png",
		isFavorit: true,
		images: [
			"http://linavilliger.ch/images/restenhose/restenhose.jpg",
		]
	}, {
		id: 4,
		name: "Die Quarantene-Bluse",
		description: "Scheint ist verband gar endlich erstieg lockere weg. Spiel das alt knapp bette gru. Licht la schau stand tiefe bello im. Gehts im ob deine dafur es ei. Hab sudwesten gro nebendran ihr schreibet dammerung schlanken erstaunen. Es spielen ab nachtun sondern starkem nachher pa braunen. Da uberall spuckte te an namlich konntet taghell. Marschen funkelte betrubte ja sa lauschte schmalen brauchen. Gesprachig gut mit geheiratet hereintrat angenommen. Wo lief beim leer nein bi ei meer mi.",
		imageURL: "http://linavilliger.ch/images/quarantenebluse/quarantene.png",
		isFavorit: true,
		images: [
			"http://linavilliger.ch/images/quarantenebluse/IMG_2565.jpg",
			"http://linavilliger.ch/images/quarantenebluse/lina.jpg",
		]
	}, {
		id: 6,
		name: "Die Taschenhose",
		description: "Scheint ist verband gar endlich erstieg lockere weg. Spiel das alt knapp bette gru. Licht la schau stand tiefe bello im. Gehts im ob deine dafur es ei. Hab sudwesten gro nebendran ihr schreibet dammerung schlanken erstaunen. Es spielen ab nachtun sondern starkem nachher pa braunen. Da uberall spuckte te an namlich konntet taghell. Marschen funkelte betrubte ja sa lauschte schmalen brauchen. Gesprachig gut mit geheiratet hereintrat angenommen. Wo lief beim leer nein bi ei meer mi.",
		imageURL: "http://linavilliger.ch/images/winterhose/winter.png",
		isFavorit: false,
		images: [
			"http://linavilliger.ch/images/taschenhose/IMG_2564.jpg",
			"http://linavilliger.ch/images/taschenhose/IMG_2568.jpg",
			"http://linavilliger.ch/images/taschenhose/tasche.jpg",
			"http://linavilliger.ch/images/taschenhose/home_a.jpg",
			"http://linavilliger.ch/images/taschenhose/IMG_2567.jpg",
		]
	}, {
		id: 9,
		name: "Das Herbstkleid",
		description: "Scheint ist verband gar endlich erstieg lockere weg. Spiel das alt knapp bette gru. Licht la schau stand tiefe bello im. Gehts im ob deine dafur es ei. Hab sudwesten gro nebendran ihr schreibet dammerung schlanken erstaunen. Es spielen ab nachtun sondern starkem nachher pa braunen. Da uberall spuckte te an namlich konntet taghell. Marschen funkelte betrubte ja sa lauschte schmalen brauchen. Gesprachig gut mit geheiratet hereintrat angenommen. Wo lief beim leer nein bi ei meer mi.",
		imageURL: "http://linavilliger.ch/images/sommerkleid/sommer.png",
		isFavorit: true,
		images: [
			"https://media1.popsugar-assets.com/files/thumbor/uHzPqGLnsVYx8NJb9rVkZAdLZSc/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2017/11/06/987/n/1922564/7e2b7d7b5a00e548324953.88136682_edit_img_image_44225135_1510005010/i/Fall-Skirts-Amazon.jpg",
		]
	}, {
		id: 10,
		name: "Die Zirkushose",
		description: "Bei diesen Hosen kam der Stoff vor dem Schnittmuster. Ich habe ihn in einem meiner Lieblings Stoffläden entdeckt und wusste sofort, dass ich daraus etwas machen wollte. Die Farben sind richtig toll in der Kombination und er ist einfach perfekt geeignet für ein Sommerkleidungsstück. Weil der Stoff so angenehm auf der Haut ist, wollte ich unbedingt auch ein Kleidungsstück nähen, dass auch angenehm zu tragen ist. Ich suchte also ein Hosenschnittmuster heraus und versuchte es so umzuzeichnen, dass ich anstelle des Reissverschlusses einen Gummizug einnähen konnte. Meine Mutter warnt mich immer davor, ein Schnittmuster abzuändern, doch genau das reizt mich noch mehr es einfach zu versuchen. Ich finde das Resultat ist anschaubar geworden.",
		imageURL: "http://linavilliger.ch/images/sonnenhose/sonnen.png",
		isFavorit: false,
		images: [
			"https://media.walbusch.ch/products/walbusch/images/394x526/EC44_3649_FA.jpg",
		]
	}, ],
	project: {},
	favorits: [],
};

// getters
const getters = {
	getProjects: state => state.projects,
	getFavorits: state => state.favorits,
	getProjectById: state => state.project,
};
// actions
const actions = {
	async loadFavorits({
		commit
	}) {
		if (state.favorits.length == 0) {
			state.projects.forEach(project => {
				if (project.isFavorit) {
					commit('ADD_FAVORIT', project);
				}

			});

		}

	},
};

// mutations
const mutations = {
	ADD_FAVORIT(state, value) {
		state.favorits.push(value);
	},
};

export default {
	state,
	getters,
	actions,
	mutations,
};