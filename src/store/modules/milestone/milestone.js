const state = {
	milestones: [{
		name: "Kantonsschule",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: false,
		startDate: "2018",
		endDate: "2022"
	}, {
		name: "Erstes Kleid",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "31.05.2020",
		endDate: null
	}, {
		name: "Erster Kragen",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "02.04.2020",
		endDate: null
	}, {
		name: "Erster Einteiler",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "13.03.2020",
		endDate: null
	}, {
		name: "Erste Hose für jemand anderes",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "06.12.2019",
		endDate: null
	}, {
		name: "Erste Hosen",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "18.07.2018",
		endDate: null
	}, {
		name: "Erster nahtverdeckter Reissverschluss",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: true,
		startDate: "25.04.2018",
		endDate: null
	}, {
		name: "Bezirksschule",
		description: "Scheint ist verband gar endlich erstieg lockere weg.",
		isMilestone: false,
		startDate: "2015",
		endDate: "2018"
	}],
};

// getters
const getters = {
	getMilestones: state => state.milestones,
};
// actions
const actions = {};

// mutations
const mutations = {};

export default {
	state,
	getters,
	actions,
	mutations,
};