import Vue from 'vue'
import Vuex from 'vuex'
import oidcSettings from '../config/oidcSettings';
import {
  vuexOidcCreateStoreModule
} from 'vuex-oidc';

Vue.use(Vuex)
import Project from './modules/project/project';
import Milestone from './modules/milestone/milestone';

export default new Vuex.Store({
  modules: {
    oidcStore: vuexOidcCreateStoreModule(oidcSettings, {
      namespaced: true,
      publicRoutePaths: ['/oidc/callback']
    }),
    Project,
    Milestone
  }
});