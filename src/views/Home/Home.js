import Wellcome from "@/components/Wellcome/Wellcome.vue";
import About from "@/components/About/About.vue";
import CV from "@/components/CV/CV.vue";
import Projects from "@/components/Projects/Projects.vue";
import Process from "@/components/Process/Process.vue";
import Navigation from "@/components/Navigation/Navigation.vue";

//import cardService from "@/services/cardService";

export default {
	name: "Home",
	props: {
		windowWidth: 0,
	},
	components: {
		Navigation,
		Wellcome,
		About,
		CV,
		Projects,
		Process
	},
	data() {
		return {
			//cards: null,
		};
	},
	//async created() {
	//this.cards = await cardService.getCards();
	//},
};