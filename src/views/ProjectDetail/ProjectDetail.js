import Wellcome from "@/components/Wellcome/Wellcome.vue";
import About from "@/components/About/About.vue";
import CV from "@/components/CV/CV.vue";

//import cardService from "@/services/cardService";

export default {
	name: "ProjectDetail",
	components: {
		Wellcome,
		About,
		CV
	},
	data() {
		return {
			//cards: null,
		};
	},
	//async created() {
	//this.cards = await cardService.getCards();
	//},
};